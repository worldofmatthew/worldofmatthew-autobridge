#!/bin/bash
## Automatic Tor Bridge Creator for Debian 10

# Pre-installation
timedatectl set-timezone Etc/UTC
apt-get update
apt-get upgrade -y
apt-get install curl sudo gnupg apt-transport-https apt unattended-upgrades apt-listchanges apt-transport-tor obfs4proxy nyx -y
#Update sources.list
echo deb https://deb.torproject.org/torproject.org buster main >> /etc/apt/sources.list.d/tor.list
echo deb-src https://deb.torproject.org/torproject.org buster main >> /etc/apt/sources.list.d/tor.list
# Add signing key
curl https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --import
gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | apt-key add 
# Install TOR
apt update
apt install tor deb.torproject.org-keyring -y
# Enable Tor at boot
systemctl enable tor
sleep 5
# Replacing TORRC with bridge config
cat /dev/null >> /etc/tor/torrc
cat <<EOT >> /etc/tor/torrc
ORPort auto
SocksPort 0
BridgeRelay 1
ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy
ServerTransportListenAddr obfs4 0.0.0.0:443
ExtOrPort auto
Log notice file /var/log/tor/notices.log
ExitPolicy reject *:*
DisableDebuggerAttachment 0
ControlPort 9051
CookieAuthentication 1

EOT
# Allow Tor Bridge to run on a port below 1024
setcap cap_net_bind_service=+ep /usr/bin/obfs4proxy
cat /dev/null >> /lib/systemd/system/tor@.service
cat <<EOT >> /lib/systemd/system/tor@.service
[Unit]
Description=Anonymizing overlay network for TCP (instance %i)
After=network.target nss-lookup.target
PartOf=tor.service
ReloadPropagatedFrom=tor.service

[Service]
Type=notify
NotifyAccess=all
PIDFile=/run/tor-instances/%i/tor.pid
PermissionsStartOnly=yes
ExecStartPre=/usr/bin/install -Z -m 02755 -o _tor-%i -g _tor-%i -d /run/tor-instances/%i
ExecStartPre=/bin/sed -e 's/@@NAME@@/%i/g; w /run/tor-instances/%i.defaults' /usr/share/tor/tor-service-defaults-torrc-instances
ExecStartPre=/usr/bin/tor --defaults-torrc /run/tor-instances/%i.defaults -f /etc/tor/instances/%i/torrc --verify-config
ExecStart=/usr/bin/tor --defaults-torrc /run/tor-instances/%i.defaults -f /etc/tor/instances/%i/torrc
ExecReload=/bin/kill -HUP ${MAINPID}
KillSignal=SIGINT
TimeoutStartSec=300
TimeoutStopSec=60
Restart=on-failure
LimitNOFILE=65536

# Hardening
NoNewPrivileges=no
PrivateTmp=yes
PrivateDevices=yes
ProtectHome=yes
ProtectSystem=full
ReadOnlyDirectories=/
# We would really like to restrict the next item to [..]/%i but we can't,
# as systemd does not support that yet.  See also #781730.
ReadWriteDirectories=-/var/lib/tor-instances
ReadWriteDirectories=-/run
CapabilityBoundingSet=CAP_SETUID CAP_SETGID CAP_NET_BIND_SERVICE CAP_DAC_READ_SEARCH

[Install]
WantedBy=multi-user.target

EOT

# Reload deamon
sleep 5
systemctl daemon-reload
# Restart Tor
sleep 1
systemctl restart tor
systemctl enable tor
#Protect Tor config from edits
sudo chattr +i /lib/systemd/system/tor@.service
sudo chattr +i /lib/systemd/system/tor@default.service

#Download replacement 50unattended-upgrades contents
cd
wget https://gitlab.com/worldofmatthew/worldofmatthew-autobridge/-/raw/master/50unattended-upgrades
# Edit Unattended-upgrades config

cat /dev/null >> /etc/apt/apt.conf.d/50unattended-upgrades
cat 50unattended-upgrades >> /etc/apt/apt.conf.d/50unattended-upgrades
# Restart Unattended-Upgrades
sleep 5
systemctl restart unattended-upgrades
sleep 20
nyx